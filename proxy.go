package main

import (
	"flag"
	"io"
	"log"
	"net/http"
	"net/url"
	"time"
)

var (
	BUF_SIZE    int
	listen_addr string
)

// struct that has to follow http.Handler interface
type proxyHandler struct {
	// shared *http.Client, can be used in multiple handlers
	client *http.Client
	// default scheme for thransparent proxy
	scheme string
}

// The only function to conform http.Handler interface
func (proxy *proxyHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	time_started := time.Now()
	proxy_request := *req // shallow copy of the original request
	// construct new URL for the proxy request
	proxy_request.URL = &url.URL{Path: req.URL.Path, RawQuery: req.URL.RawQuery}
	if req.URL.Host != "" {
		// normal proxy mode
		proxy_request.URL.Host = req.URL.Host
		proxy_request.URL.Scheme = req.URL.Scheme
	} else if req.Host != listen_addr {
		// transparent proxy mode
		proxy_request.URL.Host = req.Host
		if proxy.scheme == "" {
			proxy.scheme = "http"
		}
		proxy_request.URL.Scheme = proxy.scheme
	} else {
		// boom! neither URL in the request, nor Host: header
		w.WriteHeader(400)
		return
	}
	proxy_request.RequestURI = ""               // should be empty
	proxy_request.Host = proxy_request.URL.Host // set Host: header
	proxy_response, err := proxy.client.Do(&proxy_request)
	if err != nil {
		log.Printf("Error when performed request to %v: %v", proxy_request.URL.Host, err)
		w.WriteHeader(500)
		return
	}
	// Copy headers from the response to the proxy request
	for proxy_header, header_values := range proxy_response.Header {
		for _, header_value := range header_values {
			w.Header().Add(proxy_header, header_value)
		}
	}
	// copy response code
	w.WriteHeader(proxy_response.StatusCode)
	// copy chunks of response body
	buf := make([]byte, BUF_SIZE)
	var bytes_downloaded int
	for {
		bytes_read, err := proxy_response.Body.Read(buf)
		w.Write(buf[:bytes_read])
		bytes_downloaded += bytes_read
		if err != nil {
			if err != io.EOF {
				log.Panic("Error reading from server: %v", err)
			}
			break
		}
	}
	req_ms := time.Now().Sub(time_started).Seconds() * 1000
	log.Printf("%v %v %v %v %v %.2f", proxy_request.Method, proxy_request.URL, proxy_request.Proto, proxy_response.StatusCode, bytes_downloaded, req_ms)
}

func init() {
	// parse command line arguments
	flag.IntVar(&BUF_SIZE, "buf_size", 4096, "Read buffer size for downloads (bytes)")
	flag.StringVar(&listen_addr, "listen", "127.0.0.1:8080", "<host>:<post> to listen at")
	flag.Parse()
}

func main() {
	// create shared http.Client
	http_client := new(http.Client)
	proxy_handler := &proxyHandler{client: http_client}
	serve_mux := http.NewServeMux()
	// map the handler to the default URL
	serve_mux.Handle("/", proxy_handler)
	server := http.Server{Addr: listen_addr, Handler: serve_mux}
	log.Printf("Listening at %v", listen_addr)
	err := server.ListenAndServe()
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
